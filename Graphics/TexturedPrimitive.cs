﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace nilladon2_1.Graphics
{
    public class TexturedPrimitive
    {
        protected Texture2D image;
        protected Vector2 position;
        protected Vector2 size;

        public TexturedPrimitive(ContentManager contentManager, string imageName, Vector2 position, Vector2 size)
        {
            image = contentManager.Load<Texture2D>(imageName);
            this.position = position;
            this.size = size;
        }

        public void Update(Vector2 deltaTranslate, Vector2 deltaScale)
        {
            position += deltaTranslate;
            size += deltaScale;
        }

        public void Draw(GraphicsDeviceManager graphics, SpriteBatch spriteBatch)
        {
            Rectangle destRect = Camera.ComputePixelRectangle(graphics, position, size);
            spriteBatch.Draw(image, destRect, Color.White);
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace nilladon2_1
{
    internal struct AllInputButtons
    {
        private const Keys keyButtonA = Keys.K;
        private const Keys keyButtonB = Keys.L;
        private const Keys keyButtonX = Keys.J;
        private const Keys keyButtonY = Keys.I;

        private const Keys keyButtonBack  = Keys.F1;
        private const Keys keyButtonStart = Keys.F2;

        private ButtonState GetState(ButtonState gamePadButtonState, Keys key)
        {
            if (Keyboard.GetState().IsKeyDown(key))
                return ButtonState.Pressed;

            if (GamePad.GetState(PlayerIndex.One).IsConnected)
                return gamePadButtonState;

            return ButtonState.Released;
        }

        public ButtonState A
        {
            get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.A, keyButtonA); }
        }

        public ButtonState B
        {
            get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.B, keyButtonB); }
        }
        
        public ButtonState X
        {
            get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.X, keyButtonX); }
        }

        public ButtonState Y
        {
            get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.Y, keyButtonY); }
        }

        public ButtonState Back
        {
            get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.Back, keyButtonBack); }
        }

        public ButtonState Start
        {
            get { return GetState(GamePad.GetState(PlayerIndex.One).Buttons.Start, keyButtonStart); }
        }
    }

    internal struct AllInputTriggers
    {
        private const Keys triggerLeft = Keys.N;
        private const Keys triggerRight = Keys.M;

        const float triggerKeyValue = 0.75f;

        private float GetTriggerState(float gamePadTrigger, Keys key)
        {
            if (Keyboard.GetState().IsKeyDown(key))
                return triggerKeyValue;

            if (GamePad.GetState(PlayerIndex.One).IsConnected)
                return gamePadTrigger;

            return 0f;
        }

        public float Left
        {
            get { return GetTriggerState(GamePad.GetState(PlayerIndex.One).Triggers.Left, triggerLeft); }
        }

        public float Right
        {
            get { return GetTriggerState(GamePad.GetState(PlayerIndex.One).Triggers.Right, triggerRight); }
        }
    }

    internal struct AllThumbSticks
    {
        private const Keys thumbstickLeftUp = Keys.W;
        private const Keys thumbstickLeftDown = Keys.S;
        private const Keys thumbstickLeftLeft = Keys.A;
        private const Keys thumbstickLeftRight = Keys.D;

        private const Keys thumbstickRightUp = Keys.W;
        private const Keys thumbstickRightDown = Keys.S;
        private const Keys thumbstickRightLeft = Keys.A;
        private const Keys thumbstickRightRight = Keys.D;

        const float keyDownValue = 0.75f;

        private Vector2 ThumbStickState(Vector2 thumbStickValue, Keys up, Keys down, Keys left, Keys right)
        {
            Vector2 r = new Vector2(0f, 0f);

            if (GamePad.GetState(PlayerIndex.One).IsConnected)
                r = thumbStickValue;
            if (Keyboard.GetState().IsKeyDown(up))
                r.Y += keyDownValue;
            if (Keyboard.GetState().IsKeyDown(down))
                r.Y -= keyDownValue;
            if (Keyboard.GetState().IsKeyDown(left))
                r.X -= keyDownValue;
            if (Keyboard.GetState().IsKeyDown(right))
                r.X += keyDownValue;

            return r;
        }

        public Vector2 Left
        {
            get { return ThumbStickState(GamePad.GetState(PlayerIndex.One).ThumbSticks.Left, thumbstickLeftUp, thumbstickLeftDown, thumbstickLeftLeft, thumbstickLeftRight); }
        }

        public Vector2 Right 
        {
            get { return ThumbStickState(GamePad.GetState(PlayerIndex.One).ThumbSticks.Right, thumbstickRightUp, thumbstickRightDown, thumbstickRightLeft, thumbstickRightRight); }
        }
    }

    static class InputWrapper
    {
        static public AllInputButtons Buttons = new AllInputButtons();
        static public AllThumbSticks ThumbSticks = new AllThumbSticks();
        static public AllInputTriggers Triggers = new AllInputTriggers();
    }
}

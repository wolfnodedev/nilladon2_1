﻿using Microsoft.Xna.Framework;

namespace nilladon2_1.Graphics
{
    static public class Camera
    {
        static private float ratio = -1f;

        static private float cameraWindowToPixelRatio(GraphicsDeviceManager graphics)
        {
            if (ratio < 0f)
                ratio = graphics.PreferredBackBufferWidth / CameraWindowWidth;

            return ratio;
        }

        static public Vector2 CameraWindowOrigin { get; set; } = Vector2.Zero;
        static public float CameraWindowWidth { get; set; } = 100f;

        static public void ComputePixelPosition(GraphicsDeviceManager graphics, Vector2 cameraPosition, out int x, out int y)
        {
            float ratio = cameraWindowToPixelRatio(graphics);

            // Convert the position to pixel space
            x = (int)(((cameraPosition.X - CameraWindowOrigin.X) * ratio) + 0.5f);
            y = (int)(((cameraPosition.Y - CameraWindowOrigin.Y) * ratio) + 0.5f);

            y = graphics.PreferredBackBufferHeight - y;
        }

        static public Rectangle ComputePixelRectangle(GraphicsDeviceManager graphics, Vector2 position, Vector2 size)
        {
            float ratio = cameraWindowToPixelRatio(graphics);

            // Convert size from camera window space to pixel space.
            int width = (int)((size.X * ratio) + 0.5f);
            int height = (int)((size.Y * ratio) + 0.5f);

            // Convert the position to pixel space
            ComputePixelPosition(graphics, position, out int x, out int y);

            // Reference position is the center
            y -= height / 2;
            x -= width / 2;

            return new Rectangle(x, y, width, height);
        }
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using nilladon2_1.Graphics;

namespace nilladon2_1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        const int windowWidth = 1000;
        const int windowHeight = 100;

        const int numObjects = 4;

        // Work with the TexturedPrimitive class
        TexturedPrimitive[] graphicsObjects;
        int currentIndex = 0;
        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            // Sets initial size of window
            graphics.PreferredBackBufferWidth = windowWidth;
            graphics.PreferredBackBufferHeight = windowHeight;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            graphicsObjects = new TexturedPrimitive[numObjects];

            Camera.CameraWindowOrigin = new Vector2(10f, 20f);
            Camera.CameraWindowWidth = 300f;

            graphicsObjects[0] = new TexturedPrimitive(Content, "champion", new Vector2(1f, 1f), new Vector2(10f, 10f));
            graphicsObjects[1] = new TexturedPrimitive(Content, "champion", new Vector2(35f, 60f), new Vector2(50f, 50f));
            graphicsObjects[2] = new TexturedPrimitive(Content, "champion", new Vector2(105f, 25f), new Vector2(10f, 10f));
            graphicsObjects[3] = new TexturedPrimitive(Content, "champion", new Vector2(90f, 60f), new Vector2(10f, 10f));
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // TODO: Add your update logic here

            // Allows the game to exit
            if (InputWrapper.Buttons.Back == ButtonState.Pressed)
                this.Exit();

            #region Fullscreen Toggle
            // This region can be made much simpler by assigning only one key to toggling and simpling
            // checking the state of IsFullScreen at the same time of button press.
            // "A" to toggle to full-screen mode
            if (InputWrapper.Buttons.A == ButtonState.Pressed)
            {
                if (!graphics.IsFullScreen)
                {
                    graphics.IsFullScreen = true;
                    graphics.ApplyChanges();
                }
            }

            // "B" toggles back to windowed mode
            if (InputWrapper.Buttons.B == ButtonState.Pressed)
            {
                if (graphics.IsFullScreen)
                {
                    graphics.IsFullScreen = false;
                    graphics.ApplyChanges();
                }
            }
            #endregion

            #region Move Objects
            if (InputWrapper.Buttons.X == ButtonState.Pressed)
                currentIndex = (currentIndex + 1) % numObjects;

            graphicsObjects[currentIndex].Update(InputWrapper.ThumbSticks.Left, InputWrapper.ThumbSticks.Right);
            #endregion

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            foreach (var o in graphicsObjects)
            {
                o.Draw(graphics, spriteBatch);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
